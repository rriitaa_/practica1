package mates;

public class Matematicas{

/**
* Genera una aproximación al número pi mediante el método de
* Montecarlo. El parámetro ‘pasos‘ indica el número de puntos
* generado.
*/

	public static double generarNumeroPi(long pasos){
		aciertos = 0;
		areaDelCuadrado = 4;
		radio = 1;

		//hacemos el bucle for
		
		for (int puntosTotales = 0; puntosTotales <= pasos; puntosTotales ++){
			long x = (Math.ramdom()*2 -1);
			long y = (Math.ramdom()*2 -1);

		if (Math.sqrt(x^2 + y^2)<=1)
		aciertos = aciertos + 1;
		}

		/* voy a hacer el calculo de pi y luego inicializo el radio debajo del
		 areaDelCuadrado, finalmente pongo el metodo return para que nos devuelva
		 el valor de pi (calculoDePi)*/

		long calculoDePi = (areaDelCuadrado*(aciertos/pasos))/radio^2;
			return calculoDePi;

		
	}
}

